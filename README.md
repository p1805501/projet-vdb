# Analyse de la faune microbienne dans plusieurs toilettes publiques

Bompard Cassandra et Gérenton Pierre

Dans le cadre d'un projet de l'UE Visualisation de données biologique (VDB), une application Shiny a été développé pour produire des graphiques sur le jeu de données *Microbial Biogeography of Public Restroom Surfaces* de G.E. Flores et S.T. Bates.

L'étude portant sur la diversité microbiologique sur plusieurs surfaces (comme la poignée de la porte, ou la cuvette des toilettes) dans les toilettes publiques de deux bâtiments à Boulder, Colorado. L'intérêt est multiple, tout d'abord mieux pouvoir décrire la diversité biologique que l'on retrouve dans des environnements intérieurs, mais aussi comprendre les enjeux biologiques liés à un lieu commun et fréquentés par tous, les toilettes.

Le travail divise en deux parties . Tout d'abord le développement d'un outil pour l'exploration des données de cette étude, permettant de visualiser ces données grâce à des graphiques. Dans un deuxième temps, l'exploration même des données avec cet outil pour montrer ce qu'on peut en apprendre. Le fruit de cette deuxième partie est ce qui a permis d'enrichir un poster.

## Contenu du Git

-   **LICENCE** : contient les informations relatives à la licence. Celle qui a été choisis est la licence Creative Commons CC BY-NC-SA,
-   **app.R** : contient l'app Shiny à éxecuter pour avoir l'application,
-   **README.md** : ce document expliquant le contenu du Git et comment lancer l'application,
-   **VDB_16S_dataset.txt** : Jeu de données contenant les comptages pour chaque échantillon,
-   **VDB_16S_metadata.txt** : Jeu de données contenant les métadonnées pour chaque échantillon.
-   **poster_VDB_Bompard_Gérenton.pdf** : Le poster (A0) produit pour la présentation finale pour l'UE VDB.

## Lancer l'application

Pour cela, vous devez avoir R avec une version \>= 4.2.2 . Vous pouvez l'installer sur ce site : <https://cran.r-project.org/>.

Exécuter la commande suivante pour lancer l'application :

``` sh
Rscript app.R 
```

Vous verrez une ligne de type :

    Listening on http://127.0.0.1:port

Copier-coller cette URL dans un navigateur et vous pourrez utiliser une application.

## Utiliser l'application

L'application se sépare en trois parties :

-   *À droite* : Vous pouvez choisir les facteurs qui vous souhaités conservés ou enlever du jeu de données initiale.
-   *Au milieu* : Le graphique choisit. Les onglets du dessus permettent de choisir le type de graphique.
-   *À gauche* : Vous pouvez choisir les paramètres pour le graphe. Cassandra Bompard et Gérenton Pierre Le contenu de ce git est placé sous licence Creative Commons CC BY-NC-SA.


Plusieurs type de graphique :
- *Histogramme* : C'est un histogramme avec les combinaisons d'environnements en abscisse et le compte brut (ou la moyenne) en ordonnée. Les barres sont divisés en fonction du rang taxonomique choisis.
- *Représentation hiérarchique* : Il y a 3 représentations hiérarchiques possibles disponibles (sunburst, treemap et icicle). Ça permet de voir l'abondance de chaque taxa avec ses sous-taxa. La coloration se fait en fonction du logarithme de l'abondance.
- *ACP* : L'ACP a été faite de manière a garder chaque échantillons possibles en réduisant les espèces. La coloration permet de voir des patterns qui se dégagent.
- *HeatMap* : La HeatMap montre la correlation entre les données trouvés pour chaque paires d'environnements choisis.

L'application est également accessible en ligne à l'adresse suivante : https://pierre-gerenton.shinyapps.io/projet-vdb/ 

## Remerciement

Nous souhaitons remercier les gens de la promotion 2022-2023 du Master Bio-Informatique 2 de Lyon pour leurs appuie.
Nous remercions aussi Julien Bompard pour son aide sur la création du poster.
